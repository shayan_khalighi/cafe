import Head from "next/head";


export default function Home() {
  return (
    <div itemScope itemType="https://schema.org/CafeOrCoffeeShop">
    <main>
     <Head >Resturant and Cafe</Head>
     <h1 itemProp="name"> Dorche Cafe</h1>
     <img src="https://fastly.4sqi.net/img/general/699x268/54077572_Dw9O3J0ud_D_DZG-GbvHMUvLwgjhYnUNn1EhZ--e2zA.jpg" itemProp="image" />
      <section itemType="http://schema.org/Product" itemScope>
      <h2 itemProp="menu"  itemScope itemType="https://schema.org/MenuItem">menu</h2>
        <article>
        <h5 itemProp="name" content="Cappuchino" > Cappuchino</h5>
        <img itemProp="image" layout='fill' src="https://www.google.com/maps/uv?pb=!1s0x3f8e07bd069d8c63%3A0x2f717050b0a16b4d!3m1!7e115!4shttps%3A%2F%2Flh5.googleusercontent.com%2Fp%2FAF1QipNVNnRIpSZQu-RJ1i83JG0j9o_TXk7nbhpEDokx%3Dw426-h320-k-no!5sdenja%20-%20Google%20Search!15sCgIgAQ&imagekey=!1e10!2sAF1QipNVNnRIpSZQu-RJ1i83JG0j9o_TXk7nbhpEDokx&hl=en&sa=X&ved=2ahUKEwjL1LKb4-TwAhWJCWMBHbsUDn0QoiowGnoECEgQAw#" />
        <p itemProp="description" content="espresso-based coffee drink that originated in Italy" >espresso-based coffee drink that originated in Italy </p>
        <section itemProp="offers" itemType="http://schema.org/Offer" itemScope>
          <meta itemProp="availability" content="https://schema.org/InStock" />
          <meta itemProp="priceCurrency" content="IRR" />
          <meta itemProp="itemCondition" content="https://schema.org/UsedCondition" />
          <span itemProp="price" content="22.99" >Price: 22.99</span>
        </section>
        </article>
        <article>
        <h5 itemProp="name" content="Latte" > Latte</h5>
        <img itemProp="image" layout='fill' src="https://nl.jura.com/-/media/global/images/coffee-recipes/images-redesign-2020/latte_macchiato_2000x1400px.jpg?la=nl&hash=AB30C8A1979C5FE601D3E3FA4FFEE6043264392C" />
        <p itemProp="description" content="Caffe latte is a coffee drink made with espresso and steamed
                milk.
" >Caffe latte is a coffee drink made with espresso and steamed
                milk.
</p>
        <section itemProp="offers" itemType="http://schema.org/Offer" itemScope>
          <meta itemProp="availability" content="https://schema.org/InStock" />
          <meta itemProp="priceCurrency" content="IRR" />
          <meta itemProp="itemCondition" content="https://schema.org/UsedCondition" />
          <span itemProp="price" content="23.99" >Price: 23.99</span>
        </section>
        </article>
        <article>
        <h5 itemProp="name" content="Americano" > Americano</h5>
        <img itemProp="image" layout='fill' src="https://misterbarish.nl/wp-content/upload_folders/misterbarish.nl/Americano-750x300.jpg" />
        <p itemProp="description" content="Caffè Americano is a type of coffee drink prepared by diluting
                an espresso with hot water, giving it a similar strength to, but
                different flavor from, traditionally brewed coffee.
" >Caffè Americano is a type of coffee drink prepared by diluting
                an espresso with hot water, giving it a similar strength to, but
                different flavor from, traditionally brewed coffee.
</p>
        <section itemProp="offers" itemType="http://schema.org/Offer" itemScope>
          <meta itemProp="availability" content="https://schema.org/InStock" />
          <meta itemProp="priceCurrency" content="IRR" />
          <meta itemProp="itemCondition" content="https://schema.org/UsedCondition" />
          <span itemProp="price" content="27.99" >Price: 27.99</span>
        </section>
        </article>
        <article>
        <h5 itemProp="name" content="Doppio" > Doppio</h5>
        <img itemProp="image" layout='fill' src="https://64.media.tumblr.com/d513fa3b4faa873684a9941d9a411864/tumblr_njpdmqFRlm1ungxeyo1_500.jpg" />
        <p itemProp="description" content="Doppio espresso is a double shot which is extracted using double
                the amount of ground coffee in a larger-sized portafilter
                basket.
" >Doppio espresso is a double shot which is extracted using double
                the amount of ground coffee in a larger-sized portafilter
                basket.
</p>
        <section itemProp="offers" itemType="http://schema.org/Offer" itemScope>
          <meta itemProp="availability" content="https://schema.org/InStock" />
          <meta itemProp="priceCurrency" content="IRR" />
          <meta itemProp="itemCondition" content="https://schema.org/UsedCondition" />
          <span itemProp="price" content="25.99" >Price: 25.99</span>
        </section>
        </article>
      </section>
      <section>
            <footer>
              <p itemProp="address">
                {" "}
                <b>Cafe. Address :</b> 3M Fl., Palladium, Tehran, Tehran
                Province, Iran.{" "}
              </p>
              <meta itemProp="ratingValue" content="4" />
    <meta itemProp="bestRating" content="5" />
    Based on <span itemProp="ratingCount">25</span> user ratings
              <p itemProp="hoursAvailable">
                Open at: <time dateTime="08:00">8 AM</time>
              </p>
              <span></span>
              <p itemProp="hoursAvailable">
                {" "}
                Close at: <time dateTime="23:00">11 PM</time>
              </p>
            </footer>
          </section>
          </main>
    </div>
  );
}
